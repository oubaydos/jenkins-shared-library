#!/usr/bin/env groovy
def call(){
    echo "pushing the docker image"
    withCredentials([usernamePassword(credentialsId: 'dockerhub', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]){
        sh "echo $PASSWORD | docker login -u $USERNAME --password-stdin"
        sh 'docker push oubaydos/temp:jenkins_file'
    }
}